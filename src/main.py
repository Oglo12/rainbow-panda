from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3

import time as ti

RAINBOW_SPEED = 0.1

COLOR_FACTOR = 0.01

COLOR_CLAMP_MAX = 0.8
COLOR_CLAMP_MIN = 0

class Game(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        #self.disableMouse()

        self.panda = Actor("models/panda-model", {"walk": "models/panda-walk4"})
        self.panda.loop("walk")
        self.panda.setScale(0.0005, 0.0005, 0.0005)
        self.panda.reparentTo(self.render)

        rotation1 = self.panda.hprInterval(5, Point3(180, 180, 180), Point3(0, 0, 0))
        rotation2 = self.panda.hprInterval(5, Point3(0, 0, 0), Point3(-180, -180, -180))

        self.pandaRotate = Sequence(rotation1, rotation2, name = "PandaRotate")
        self.pandaRotate.loop()

        self.rainbowPrevTime = ti.time()

        self.colors = [0.5, 0.2, 0.0]
        self.modes = [".", ".", "."]

        self.taskMgr.add(self.rainbowPandaTask, "RainbowPanda")

        self.panda.setPos(0, 2, 0)

    def rainbowPandaTask(self, task):
        for i in range(len(self.colors)):
            if self.colors[i] <= COLOR_CLAMP_MIN:
                self.modes[i] = "+"
            if self.colors[i] >= COLOR_CLAMP_MAX:
                self.modes[i] = "-"

        self.panda.setColor(self.colors[0], self.colors[1], self.colors[2])

        if ti.time() - self.rainbowPrevTime > RAINBOW_SPEED:
            for i in range(len(self.modes)):
                if self.modes[i] == "+":
                    self.colors[i] += COLOR_FACTOR
                else:
                    self.colors[i] -= COLOR_FACTOR

            self.rainbowPrevTime = ti.time()

        return task.cont

app = Game()
app.run()
